export class Login {
  constructor(email, password) {
    this._email = email;
    this._password = password;
    this.myurl = 'https://semantic-ui.com/examples/login.html';
    this.visit();
    this.populate();
    this.submit();
  }

  visit = () => {
    cy.visit(this.myurl);
    cy.contains("Log-in to your account");
    cy.contains('New to us? Sign Up');
  };

  populate = () => {
    cy.get('[name=email]').type(this._email)
        .should('have.value', this._email);
    cy.get('[name=password]').type(this._password)
        .should('have.value', this._password);
  };

  submit = () => {
    cy.contains('Login').click();
  }
}